import * as api from './api';
import {
    dict,
    UserPageQuery,
    AddReq,
    DelReq,
    EditReq,
    compute,
    CreateCrudOptionsProps,
    CreateCrudOptionsRet
} from '@fast-crud/fast-crud';
import {request} from '/@/utils/service';
import {dictionary} from '/@/utils/dictionary';
import {successMessage} from '/@/utils/message';
import {auth} from '/@/utils/authFunction';
import {SystemConfigStore} from "/@/stores/systemConfig";
import {storeToRefs} from "pinia";
import {computed} from "vue";
import { Md5 } from 'ts-md5';
import {commonCrudConfig} from "/@/utils/commonCrud";
export const createCrudOptions = function ({crudExpose}: CreateCrudOptionsProps): CreateCrudOptionsRet {

    const pageRequest = async (query: UserPageQuery) => {
        return await api.GetList(query);
    };
    const editRequest = async ({form, row}: EditReq) => {
        form.id = row.id;
        return await api.UpdateObj(form);
    };
    const delRequest = async ({row}: DelReq) => {
        return await api.DelObj(row.id);
    };
    const addRequest = async ({form}: AddReq) => {
        return await api.AddObj(form);
    };

    const exportRequest = async (query: UserPageQuery) => {
        return await api.exportData(query)
    }

    const systemConfigStore = SystemConfigStore()
    const {systemConfig} = storeToRefs(systemConfigStore)
    const getSystemConfig = computed(() => {
        console.log(systemConfig.value)
        return systemConfig.value
    })


    return {
        crudOptions: {
            table: {
                remove: {
                    confirmMessage: '是否删除该用户？',
                },
            },
            request: {
                pageRequest,
                addRequest,
                editRequest,
                delRequest,
            },
            form: {
                initialForm: {
                    password: computed(() => {
                        return systemConfig.value['base.default_password']
                    }),
                }
            },
            actionbar: {
                buttons: {
                    add: {
                        show: auth('user:Create')
                    },
                    export: {
                        text: "导出",//按钮文字
                        title: "导出",//鼠标停留显示的信息
                        click() {
                            return exportRequest(crudExpose!.getSearchFormData())
                        }
                    }
                }
            },
            rowHandle: {
                //固定右侧
                fixed: 'right',
                width: 200,
                buttons: {
                    view: {
                        show: true,
                    },
                    edit: {
                        iconRight: 'Edit',
                        type: 'text',
                        show: auth('user:Update'),
                    },
                    remove: {
                        iconRight: 'Delete',
                        type: 'text',
                        show: auth('user:Delete'),
                    },
                    // custom: {
                    //     text: '重设密码',
                    //     type: 'text',
                    //     show: auth('user:ResetPassword'),
                    //     tooltip: {
                    //         placement: 'top',
                    //         content: '重设密码',
                    //     },
                    //     //@ts-ignore
                    //     click: (ctx: any) => {
                    //         const {row} = ctx;
                    //     },
                    // },
                },
            },
            columns: {
                _index: {
                    title: '序号',
                    form: {show: false},
                    column: {
                        type: 'index',
                        align: 'center',
                        width: '70px',
                        columnSetDisabled: true, //禁止在列设置中选择
                    },
                },
                // 调整columns中的week配置
                week: {
                    title: '星期',
                    search: {show: true},
                    type: 'dict-select',
                    column: {
                        width: 120,
                        sortable: true,
                    },
                    dict: dict({
                        data: [
                            {label: '星期日', value: 0},
                            {label: '星期一', value: 1, color: 'success'},
                            {label: '星期二', value: 2, color: 'warning'},
                            {label: '星期三', value: 3, color: 'danger'},
                            {label: '星期四', value: 4, color: 'success'},
                            {label: '星期五', value: 5, color: 'warning'},
                            {label: '星期六', value: 6, color: 'danger'},
                        ],
                    }),
                    form: {
                        rules: [{required: true, message: '必填项'}],
                    },
                },
                doctor: {
                    title: '医生',
                    search: {
                        show: true,
                    },
                    type: 'dict-tree',
                    dict: dict({
                        isTree: true,
                        url: '/api/system/user/',
                        value: 'id',
                        label: 'name'
                    }),
                    column: {
                        minWidth: 150, //最小列宽
                    },
                    form: {
                        rules: [
                            // 表单校验规则
                            {
                                required: true,
                                message: '必填项',
                            },
                        ],
                        component: {
                            filterable: true,
                            placeholder: '请选择',
                            props: {
                                checkStrictly:true,
                                props: {
                                    value: 'id',
                                    label: 'name',
                                },
                            },
                        },
                    },
                },
                department: {
                    title: '部门',
                    search: {
                        show: true,
                    },
                    type: 'dict-tree',
                    dict: dict({
                        isTree: true,
                        url: '/api/system/dept/all_dept/',
                        value: 'id',
                        label: 'name'
                    }),
                    column: {
                        minWidth: 150, //最小列宽
                    },
                    form: {
                        rules: [
                            // 表单校验规则
                            {
                                required: true,
                                message: '必填项',
                            },
                        ],
                        component: {
                            filterable: true,
                            placeholder: '请选择',
                            props: {
                                checkStrictly:true,
                                props: {
                                    value: 'id',
                                    label: 'name',
                                },
                                color: 'success',
                            },
                        },
                    },
                },
                noon_code: {
                    title: '午别',
                    search: {
                        show: true,
                    },
                    type: 'dict-select',
                    column: {
                        width: 120,
                        sortable: true,
                    },
                    dict: dict({
                        data: [
                            {label: '上午', value: 1, color: 'success'},
                            {label: '下午', value: 2, color: 'warning'},
                            {label: '晚上', value: 3, color: 'danger'},
                        ],
                    }),
                    form: {
                        rules: [
                            // 表单校验规则
                            {
                                required: true,
                                message: '午别必填项',
                            },
                        ],
                        component: {
                            span: 12,
                            placeholder: '请输入午别',
                        },
                    },
                },
                begin_time: {
                    title: '排班开始时间',
                    type: 'time-picker',
                    column: {
                        minWidth: 90,
                    },
                    search: { // 确保搜索功能启用，并配置日期选择器作为搜索组件
                        disabled: false,
                        component: {
                            name: 'time-picker', // 使用日期选择器组件'time-picker'
                            props: {
                                clearable: true,
                                placeholder: '选择排班开始时间搜索',
                            },
                        },
                    },
                    form: {
                        
                        component: {
                            placeholder: '请输入排班开始时间',
                        },
                    },
                    component: { props: { color: 'auto' } }, 
                },
                end_time: {
                    title: '排班结束时间',
                    type: 'time-picker',
                    column: {
                        minWidth: 90,
                    },
                    search: { // 确保搜索功能启用，并配置日期选择器作为搜索组件
                        disabled: false,
                        component: {
                            name: 'time-picker', // 使用日期选择器组件
                            props: {
                                clearable: true,
                                type: 'datetime', // 或'datetime'根据需求选择日期或日期时间类型
                                placeholder: '选择排班结束时间搜索',
                            },
                        },
                    },
                    form: {
                        component: {
                            placeholder: '请输入排班结束时间',
                        },
                    },
                    component: { props: { color: 'auto' } }, 
                },
                valid_flag: {
					title: '是否有效',
					search: {
						show: true,
					},
					type: 'dict-radio',
					column: {
						minWidth: 90,
						component: {
							name: 'fs-dict-switch',
							activeText: '',
							inactiveText: '',
							style: '--el-switch-on-color: var(--el-color-primary); --el-switch-off-color: #dcdfe6',
							onChange: compute((context) => {
								return () => {
									api.UpdateObj(context.row).then((res: APIResponseData) => {
										successMessage(res.msg as string);
									});
								};
							}),
						},
					},
					dict: dict({
						data: dictionary('button_status_bool'),
					}),
				},
                ...commonCrudConfig({
					begin_time: {
						search: true
					}
				})
            },
        },
    };
};
