import { request } from '/@/utils/service';

export const apiPrefix = '/api/director/visit/';
export function GetList() {
	return request({
		url: apiPrefix,
		method: 'get',
	});
}
// export function GetObj(id: InfoReq) {
// 	return request({
// 		url: apiPrefix + id,
// 		method: 'get',
// 	});
// }

// export function AddObj(obj: AddReq) {
// 	return request({
// 		url: apiPrefix,
// 		method: 'post',
// 		data: obj,
// 	});
// }

// export function UpdateObj(obj: EditReq) {
// 	return request({
// 		url: apiPrefix + obj.id + '/',
// 		method: 'put',
// 		data: obj,
// 	});
// }

// export function DelObj(id: DelReq) {
// 	return request({
// 		url: apiPrefix + id + '/',
// 		method: 'delete',
// 		data: { id },
// 	});
// }
