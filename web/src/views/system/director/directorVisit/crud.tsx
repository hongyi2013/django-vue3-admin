import * as api from './api';
import { UserPageQuery, AddReq, DelReq, EditReq, CreateCrudOptionsProps, CreateCrudOptionsRet, dict } from '@fast-crud/fast-crud';
import {commonCrudConfig} from "/@/utils/commonCrud";

export const createCrudOptions = function ({ crudExpose }: CreateCrudOptionsProps): CreateCrudOptionsRet {
	const pageRequest = async () => {
		return await api.GetList();
	};
	return {
		crudOptions: {
			request: {
				pageRequest,
			},
			actionbar: {
				buttons: {
					add: {
						show: false,
					},
				},
			},
			rowHandle: {
				fixed:'right',
				width: 100,
				buttons: {
					view: {
						type: 'text',
					},
					edit: {
						show: false,
					},
					remove: {
						show: false,
					},
				},
			},
			columns: {
				_index: {
					title: '序号',
					form: { show: false },
					column: {
						//type: 'index',
						align: 'center',
						width: '70px',
						columnSetDisabled: true, //禁止在列设置中选择
						formatter: (context) => {
							//计算序号,你可以自定义计算规则，此处为翻页累加
							let index = context.index ?? 1;
							let pagination = crudExpose!.crudBinding.value.pagination;
							return ((pagination!.currentPage ?? 1) - 1) * pagination!.pageSize + index + 1;
						},
					},
				},
				search: {
					title: '关键词',
					column: {
						show: false,
					},
					search: {
						show: true,
						component: {
							props: {
								clearable: true,
							},
							placeholder: '请输入关键词',
						},
					},
					form: {
						show: false,
						component: {
							props: {
								clearable: true,
							},
						},
					},
				},
				clinic_code: {
					title: '就医登记号',
					search: {
						disabled: false,
					},
					type: 'input',
					column:{
						minWidth: 120,
					},
					form: {
						disabled: true,
						component: {
							placeholder: '请输入就医登记号',
						},
					},
				},
                name: {
                    title: '患者姓名',
                    search: {
                        disabled: true, // 确保搜索功能启用
                        component: { // 添加或调整搜索框组件配置
                            name: 'input', // 假设使用默认输入框组件
                            props: {
                                clearable: true,
                                placeholder: '请输入患者姓名搜索',
                            },
                        },
                    },
                    type: 'input',
                    column: {
                        minWidth: 120,
                    },
                    form: {
                        disabled: true,
                        component: {
                            placeholder: '请输入患者姓名',
                        },
                    },
                },
				age: {
					title: '年龄',
					search: {
						disabled: false,
					},
					disabled: true,
					type: 'input',
					column:{
						minWidth: 120,
					},
					form: {
						component: {
							placeholder: '请输入年龄',
						},
					},
				},
				phone: {
					title: '手机号码',
					type: 'input',
					column:{
						minWidth: 90,
					},
					form: {
						disabled: true,
						component: {
							placeholder: '请输入手机号码',
						},
					},
					component: { props: { color: 'auto' } }, // 自动染色
				},
                begin_time: {
                    title: '挂号开始时间',
                    type: 'input',
                    column: {
                        minWidth: 90,
                    },
                    search: { // 确保搜索功能启用，并配置日期选择器作为搜索组件
                        disabled: false,
                        component: {
                            name: 'date-picker', // 使用日期选择器组件
                            props: {
                                clearable: true,
                                type: 'date', // 或'datetime'根据需求选择日期或日期时间类型
                                placeholder: '选择挂号开始时间搜索',
                            },
                        },
                    },
                    form: {
                        component: {
                            placeholder: '请输入挂号开始时间',
                        },
                    },
                    component: { props: { color: 'auto' } }, 
                },
				oper_date: {
					title: '操作时间',
					type: 'input',
					column:{
						minWidth: 80,
					},
					form: {
						component: {
							placeholder: '请输入操作时间',
						},
					},
					component: { props: { color: 'auto' } }, // 自动染色
				},
				...commonCrudConfig({
					begin_time: {
						search: true
					}
				})
			},
		},
	};
};
