from django.urls import path
from rest_framework import routers

from dvadmin.director_visit.views.director import VisisInfoViewSet

# system_url = routers.SimpleRouter()
# system_url.register(r'visit', VisisInfoViewSet) # 院长出诊查询
urlpatterns = [
    path('visit/', VisisInfoViewSet.as_view({'get': 'list'})),
    path('visit/<int:pk>/', VisisInfoViewSet.as_view({'get': 'retrieve'})),
    # path('visit/info', VisisInfoView.as_view()),
    # path('user/import/', UserViewSet.as_view({'get': 'import_data', 'post': 'import_data'})),
]


# urlpatterns += system_url.urls