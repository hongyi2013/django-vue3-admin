from django.apps import AppConfig


class DirectorVisitConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dvadmin.director_visit'
