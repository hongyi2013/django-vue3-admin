from django.db import models

# Create your models here.

class VDirectorVisit(models.Model):
    """院长挂号患者表"""
    clinic_code = models.CharField(primary_key=True, max_length=48)
    name = models.CharField(max_length=48, blank=True, null=True)
    age = models.CharField(max_length=256, blank=True, null=True)
    sex = models.CharField(max_length=256, blank=True, null=True)
    phone = models.CharField(max_length=2048, blank=True, null=True)
    begin_time = models.DateTimeField(max_length=256, blank=True, null=True)
    oper_date = models.DateTimeField(max_length=2048, blank=True, null=True)
    enable = models.BooleanField(default=True, verbose_name="启用状态")

    class Meta:
        managed = False
        db_table = 'v_director_visit'