from django.shortcuts import render

# Create your views here.

from rest_framework.views import APIView
from rest_framework.response import Response
from dvadmin.director_visit.models import VDirectorVisit
from rest_framework import serializers
from dvadmin.utils.json_response import SuccessResponse, ErrorResponse, DetailResponse
from dvadmin.utils.serializers import CustomModelSerializer
from dvadmin.utils.viewset import CustomModelViewSet


class VDirectorVisitSerializer(CustomModelSerializer):
    class Meta:
        model = VDirectorVisit
        fields = "__all__"


class VisisInfoViewSet(CustomModelViewSet):
    """
    院长挂号患者表"
    list:查询
    create:新增
    update:修改
    retrieve:单例
    destroy:删除
    """
    queryset = VDirectorVisit.objects.using("hislp").all()
    serializer_class = VDirectorVisitSerializer
    extra_filter_class = []

    def get_queryset(self):
        self.request.query_params._mutable = True
        params = self.request.query_params
        pcode = params.get('pcode', None)
        page = params.get('page', None)
        limit = params.get('limit', None)
        if page:
            del params['page']
        if limit:
            del params['limit']
        if params and pcode:
            queryset = self.queryset.filter(enable=True, pcode=pcode)
        else:
            queryset = self.queryset.filter(enable=True)
        return queryset
