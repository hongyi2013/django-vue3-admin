from django.db.models import Q
from django.shortcuts import render
from rest_framework.permissions import AllowAny

from dvadmin.blood_queuing.models import VBloodPatientinfo,Schema,Triage,Terminal
from dvadmin.utils.serializers import CustomModelSerializer
from dvadmin.utils.viewset import CustomModelViewSet
from rest_framework.decorators import action
from dvadmin.utils.json_response import SuccessResponse, ErrorResponse, DetailResponse

class VBloodPatientinfoSerializer(CustomModelSerializer):
    class Meta:
        model = VBloodPatientinfo
        fields = "__all__"


class VBloodPatientinfoViewSet(CustomModelViewSet):
    """
    抽血患者表
    list:查询
    create:新增
    update:修改
    retrieve:单例
    destroy:删除
    """
    queryset = VBloodPatientinfo.objects.using("hislp").all()
    # queryset = VBloodPatientinfo.objects.all()
    serializer_class = VBloodPatientinfoSerializer
    extra_filter_class = []
    # 允许所有用户（包括匿名用户）访问此ViewSet的所有动作
    permission_classes = [AllowAny]

    def get_queryset(self):
        self.request.query_params._mutable = True
        params = self.request.query_params
        pcode = params.get('pcode', None)
        page = params.get('page', None)
        limit = params.get('limit', None)
        if page:
            del params['page']
        if limit:
            del params['limit']
        if params and pcode:
            queryset = self.queryset.filter(enable=True, pcode=pcode)
        else:
            queryset = self.queryset.filter(enable=True)
        return queryset


    @action(detail=False, methods=['get'])
    def count_by_idenno(self, request):
        # 1.判断当前时间是否可以抽血排队
        # 2.判断是否有抽血的记录
        # 3.在排队表插入数据
        # 4.发送ws接口到前端
        # 5.返回数据。

        # 2.抽血患者表，判断是否有抽血的记录
        idenno = request.query_params.get('idenno')
        if idenno is None or idenno == '':
            return ErrorResponse({"error": "请输入证件号码"}, status=400)
        count = self.queryset.filter(
            Q(idenno=idenno) | Q(card_no=idenno)
        ).count()
        if count==0:
            return ErrorResponse({"error": "未查询到就诊记录"}, status=400)
        return SuccessResponse({"count": count})


class SchemaSerializer(CustomModelSerializer):
    """
    医生排班接口-序列化器
    """
    class Meta:
        model = Schema
        fields = "__all__"
        read_only_fields = ["id"]


class SchemaCreateUpdateSerializer(CustomModelSerializer):
    """
    初始化菜单按钮-序列化器
    """
    class Meta:
        model = Schema
        fields = "__all__"
        read_only_fields = ["id"]


class SchemaViewSet(CustomModelViewSet):
    """
    医生排班接口
    list:查询
    create:新增
    update:修改
    retrieve:单例
    destroy:删除
    """
    queryset = Schema.objects.all()
    serializer_class = SchemaSerializer
    create_serializer_class = SchemaCreateUpdateSerializer
    update_serializer_class = SchemaCreateUpdateSerializer
    extra_filter_class = []

    # 允许所有用户（包括匿名用户）访问此ViewSet的所有动作
    permission_classes = [AllowAny]


class TriageSerializer(CustomModelSerializer):
    """
    分诊区域接口-序列化器
    """
    class Meta:
        model = Triage
        fields = "__all__"
        read_only_fields = ["id"]


class TriageCreateUpdateSerializer(CustomModelSerializer):
    """
    初始化菜单按钮-序列化器
    """
    class Meta:
        model = Triage
        fields = "__all__"
        read_only_fields = ["id"]


class TriageViewSet(CustomModelViewSet):
    """
    分诊区域接口
    list:查询
    create:新增
    update:修改
    retrieve:单例
    destroy:删除
    """
    queryset = Triage.objects.all()
    serializer_class = TriageSerializer
    create_serializer_class = TriageCreateUpdateSerializer
    update_serializer_class = TriageCreateUpdateSerializer
    extra_filter_class = []

    # 允许所有用户（包括匿名用户）访问此ViewSet的所有动作
    permission_classes = [AllowAny]




class TerminalSerializer(CustomModelSerializer):
    """
    分诊区域接口-序列化器
    """
    class Meta:
        model = Terminal
        fields = "__all__"
        read_only_fields = ["id"]


class TerminalCreateUpdateSerializer(CustomModelSerializer):
    """
    初始化菜单按钮-序列化器
    """
    class Meta:
        model = Terminal
        fields = "__all__"
        read_only_fields = ["id"]


class TerminalViewSet(CustomModelViewSet):
    """
    设备表
    list:查询
    create:新增
    update:修改
    retrieve:单例
    destroy:删除
    """
    queryset = Terminal.objects.all()
    serializer_class = TerminalSerializer
    create_serializer_class = TerminalCreateUpdateSerializer
    update_serializer_class = TerminalCreateUpdateSerializer
    extra_filter_class = []

    # 允许所有用户（包括匿名用户）访问此ViewSet的所有动作
    permission_classes = [AllowAny]