from django.db import models
from dvadmin.utils.models import CoreModel, table_prefix, get_custom_app_models
from dvadmin.system.models import Users,Dept
# Create your models here.
table_prefix_blood = 'blood'

class VBloodPatientinfo(models.Model):
    """近五天有效抽血患者信息"""
    pk_recipe_no = models.CharField(primary_key=True, max_length=48, verbose_name="唯一id")
    name = models.CharField(max_length=48, blank=True, null=True, verbose_name="患者姓名")
    idenno = models.CharField(max_length=256, blank=True, null=True, verbose_name="患者身份证号码")
    card_no = models.CharField(max_length=256, blank=True, null=True, verbose_name="患者卡号")
    # item_name = models.CharField(max_length=2048, blank=True, null=True, verbose_name="收费名称")
    oper_date = models.DateTimeField(blank=True, null=True, verbose_name="收费时间")
    exec_dpcd = models.CharField(max_length=2048, blank=True, null=True, verbose_name="执行科室代码")
    exec_dpnm = models.CharField(max_length=2048, blank=True, null=True, verbose_name="执行科室名称")
    enable = models.BooleanField(default=True, verbose_name="启用状态")

    class Meta:
        managed = False
        db_table = 'v_blood_patientinfo'


class Schema(CoreModel):
    """医生排班信息"""
    WEEK_CHOICES = (
        (0, "星期日"),
        (1, "星期一"),
        (2, "星期二"),
        (3, "星期三"),
        (4, "星期四"),
        (5, "星期五"),
        (6, "星期六"),
    )
    week = models.IntegerField(choices=WEEK_CHOICES, default="0",  verbose_name="星期几", help_text="星期几")
    doctor = models.ForeignKey(
        to=Users,  # 正确引用Users模型
        on_delete=models.CASCADE,
        verbose_name="医生",
        related_name="schedules",
        help_text="医生",
    )
    department = models.ForeignKey(
        to=Dept,  # 正确引用Dept模型
        on_delete=models.CASCADE,
        verbose_name="科室",
        related_name="department_schedules",
        help_text="科室",
    )

    doct_code = models.CharField(max_length=24, verbose_name="医生代码",null=True, help_text="医生代码")
    doct_name = models.CharField(max_length=48, verbose_name="医生姓名",null=True, help_text="医生姓名")
    # doct =

    doct_type = models.CharField(max_length=24, verbose_name="医生类别", null=True, help_text="医生类别")
    noon_code = models.CharField(max_length=32, verbose_name="午别", help_text="午别")
    begin_time = models.TimeField(max_length=32, verbose_name="开始时间", help_text="开始时间")
    end_time = models.TimeField(max_length=32, verbose_name="结束时间", help_text="结束时间")
    reg_lmt = models.CharField(max_length=32, verbose_name="挂号限额", null=True, help_text="挂号限额")
    valid_flag = models.BooleanField(default=True, verbose_name="是否有效")
    enable = models.BooleanField(default=True, verbose_name="启用状态")
    # STATUS_CHOICES = (
    #     (0, "离职"),
    #     (1, "在职"),
    # )
    # status = models.IntegerField(choices=STATUS_CHOICES, default=1, verbose_name="岗位状态", help_text="岗位状态")

    class Meta:
        db_table = table_prefix + "schema"
        verbose_name = "医生排班表"
        verbose_name_plural = verbose_name
        # ordering = ("sort",)

    def __str__(self):
        return f"{self.doctor.name}在{self.department.name}的排班"


class Triage(CoreModel):
    """分诊区域位置表 """
#     """
#     SELECT t.triage_id,t.triage_type,t.pager_type,t.call_buffer,t.name,t.ip,
# t.description,t.reorder_type,t.return_flag_step,t.triage_pwd
#
#  FROM triage t
#     """
    triage_type = models.IntegerField(verbose_name="分诊区域类型",null=True, help_text="医生代码")
    pager_type = models.IntegerField(verbose_name="区域类型",null=True, help_text="医生代码")
    call_buffer = models.IntegerField(verbose_name="区域类型",null=True, help_text="医生代码")
    name = models.CharField(max_length=24, verbose_name="分诊区域名称",null=True, help_text="医生代码")
    ip_addr = models.CharField(max_length=24, verbose_name="ip地址",null=True, help_text="医生代码")
    description = models.CharField(max_length=24, verbose_name="描述信息",null=True, help_text="医生代码")
    reorder_type = models.IntegerField(verbose_name="区域类型",null=True, help_text="医生代码")
    return_flag_step = models.IntegerField( verbose_name="区域类型",null=True, help_text="医生代码")
    triage_pwd = models.CharField(max_length=24, verbose_name="区域密码",null=True, help_text="医生代码")
    valid_flag = models.BooleanField(default=True, verbose_name="是否有效")
    enable = models.BooleanField(default=True, verbose_name="启用状态")

    class Meta:
        db_table = table_prefix + "triage"
        verbose_name = "分诊区域位置表"
        verbose_name_plural = verbose_name
        # ordering = ("sort",)

    def __str__(self):
        return f"{self.name}的ip地址是{self.ip_addr}的排班"


# SELECT terminal.mac,terminal.triage_id,terminal.name FROM terminal te

class Terminal(CoreModel):
    """叫号设备表 """
#     """
# SELECT terminal.mac,terminal.triage_id,terminal.name FROM terminal te
#     """
    triage_id = models.ForeignKey(
        to="Triage",
        verbose_name="所属分诊区域",
        on_delete=models.PROTECT,
        db_constraint=False,
        null=True,
        blank=True,
        help_text="关联分诊区域",
    )

    TERMINAL_TYPE_CHOICES = (
        (0, "患者报到机"),
        (1, "大电视机显示屏"),
    )
    # week = models.IntegerField(choices=WEEK_CHOICES, default="0",  verbose_name="星期几", help_text="星期几")

    terminal_type = models.IntegerField(choices=TERMINAL_TYPE_CHOICES,verbose_name="设备类型",null=True, help_text="设备类型")
    mac = models.CharField(max_length=24, verbose_name="mac地址",null=True, help_text="mac地址")
    name = models.CharField(max_length=24, verbose_name="设备名称",null=True, help_text="设备名称")
    display_name = models.CharField(max_length=24, verbose_name="备注",null=True, help_text="备注")
    valid_flag = models.BooleanField(default=True, verbose_name="是否有效")
    enable = models.BooleanField(default=True, verbose_name="启用状态")

    class Meta:
        db_table = table_prefix + "terminal"
        verbose_name = "叫号设备表"
        verbose_name_plural = verbose_name
        # ordering = ("sort",)

    def __str__(self):
        return f"{self.name}的ip地址是{self.mac}的排班"