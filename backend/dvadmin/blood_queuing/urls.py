from django.urls import path
from rest_framework import routers

from dvadmin.blood_queuing.views.blood import VBloodPatientinfoViewSet, SchemaViewSet, TriageViewSet,TerminalViewSet

blood_url = routers.SimpleRouter()
blood_url.register(r'schema', SchemaViewSet)
blood_url.register(r'triage', TriageViewSet)
blood_url.register(r'terminal', TerminalViewSet)

urlpatterns = [
    path('blood_patient/', VBloodPatientinfoViewSet.as_view({'get': 'list'})),
    path('blood_patient/<int:pk>/', VBloodPatientinfoViewSet.as_view({'get': 'retrieve'})),
    path('blood_patient/count_by_idenno/', VBloodPatientinfoViewSet.as_view({'get': 'count_by_idenno'}),
         name='blood_patient_count_by_idenno'),
]


urlpatterns += blood_url.urls
# /api/blood/schema/