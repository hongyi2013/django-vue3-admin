from django.apps import AppConfig


class BloodQueuingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dvadmin.blood_queuing'
